﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AeonInventory.Models;

namespace AeonInventory.Controllers
{
    public class RolesMastersController : Controller
    {
        private DBContext db = new DBContext();

        // GET: RolesMasters
        public ActionResult Index()
        {
            ViewBag.RolesCount = db.Inventories.Where(x => x.InventoryType == "Roles").FirstOrDefault().Quantity;
            return View(db.RolesMaster.ToList());
        }

        // GET: RolesMasters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesMaster rolesMaster = db.RolesMaster.Find(id);
            if (rolesMaster == null)
            {
                return HttpNotFound();
            }
            return View(rolesMaster);
        }

        // GET: RolesMasters/Create
        public ActionResult Create()
        {
            if(db.Inventories.Any(x => x.InventoryType == "Roles"))
            {
                ViewBag.RolesCount = db.Inventories.Where(x => x.InventoryType == "Roles").FirstOrDefault().Quantity;
            }
            
            return View();
        }

        // POST: RolesMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,WaybillNo,Date,RolesAssigned,TicketsSold")] RolesMaster rolesMaster)
        {
            if (ModelState.IsValid)
            {
                db.RolesMaster.Add(rolesMaster);
                db.SaveChanges();
                Inventory inventories = new Inventory();
                inventories = db.Inventories.Where(x => x.InventoryType == "Roles" ).FirstOrDefault();
                inventories.Quantity -= rolesMaster.RolesAssigned;              
                db.Entry(inventories).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rolesMaster);
        }

        // GET: RolesMasters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesMaster rolesMaster = db.RolesMaster.Find(id);
            rolesMaster.lastRolesAssigned = rolesMaster.RolesAssigned;
            if (rolesMaster == null)
            {
                return HttpNotFound();
            }
            return View(rolesMaster);
        }

        // POST: RolesMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,WaybillNo,Date,RolesAssigned,TicketsSold,lastRolesAssigned")] RolesMaster rolesMaster)
        {
            if (ModelState.IsValid)
            {
                //int prevcount = db.RolesMaster.Find(rolesMaster.id).RolesAssigned;               
                if(rolesMaster.lastRolesAssigned > rolesMaster.RolesAssigned)
                {
                    int currntcount = rolesMaster.lastRolesAssigned - rolesMaster.RolesAssigned;
                    db.Entry(rolesMaster).State = EntityState.Modified;
                    
                    db.SaveChanges();
                    Inventory inventories = new Inventory();
                    inventories = db.Inventories.Where(x => x.InventoryType == "Roles").FirstOrDefault();
                    inventories.Quantity += currntcount;
                    db.Entry(inventories).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    int currntcount = rolesMaster.RolesAssigned - rolesMaster.lastRolesAssigned;
                    db.Entry(rolesMaster).State = EntityState.Modified;
                    db.SaveChanges();
                    Inventory inventories = new Inventory();
                    inventories = db.Inventories.Where(x => x.InventoryType == "Roles").FirstOrDefault();
                    inventories.Quantity -= currntcount;
                    db.Entry(inventories).State = EntityState.Modified;
                    db.SaveChanges();
                }
               
                return RedirectToAction("Index");
            }
            return View(rolesMaster);
        }

        // GET: RolesMasters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesMaster rolesMaster = db.RolesMaster.Find(id);
            if (rolesMaster == null)
            {
                return HttpNotFound();
            }
            return View(rolesMaster);
        }

        // POST: RolesMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RolesMaster rolesMaster = db.RolesMaster.Find(id);
            db.RolesMaster.Remove(rolesMaster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
