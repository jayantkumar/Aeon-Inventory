﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AeonInventory.Models;

namespace AeonInventory.Controllers
{
    public class InventoriesController : Controller
    {
        private DBContext db = new DBContext();

        // GET: Inventories
        
        //public ActionResult Index()
        //{
        //    ViewBag.Locations = new SelectList(db.LocationMasters, "Name", "Name");
        //    return View(db.Inventories.ToList());
        //}
        //[HttpPost]
        public ActionResult Index(string param)
        {
            ViewBag.Locations = new SelectList(db.LocationMasters, "Name", "Name");
            if(param!=null)
            {
                //return View(db.Inventories.Where(x => x.Location == param).ToList());
                if(param!="Select")
                {
                    return PartialView("InventoryDetails", db.Inventories.Where(x => x.Location == param).ToList());
                }
                else
                {
                    return PartialView("InventoryDetails", db.Inventories.ToList());
                }
                
            }
            //return PartialView("InventoryDetails", db.Inventories.ToList());
            return View(db.Inventories.ToList());
        }

        // GET: Inventories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // GET: Inventories/Create
        public ActionResult Create()
        {
            ViewBag.Inventorytype = new SelectList(db.InventoryTypeMasters, "Type", "Type");
            ViewBag.Locations = new SelectList(db.LocationMasters, "Name", "Name");
            return View();
        }

        // POST: Inventories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Name,InventoryType,InventoryCode,Configuration,Vendor,PurchaseDate,Quantity")] Inventory inventory)
        {
            if (ModelState.IsValid)
            {
                var Location = inventory.InventoryCode;
                var id = 1;
                if (inventory.InventoryType=="Roles")
                {
                    if(db.Inventories.Any(x => x.InventoryType == inventory.InventoryType && x.Location == Location))
                    {
                        Inventory inventories = new Inventory();
                        inventories = db.Inventories.Where(x => x.InventoryType == inventory.InventoryType && x.Location == Location).FirstOrDefault();
                        inventories.Quantity += inventory.Quantity;
                        inventories.PurchaseDate = inventory.PurchaseDate;
                        db.Entry(inventories).State = EntityState.Modified;
                        db.SaveChanges();

                    }                 
                    
                }
                else
                {
                    while (inventory.Quantity > 0)
                    {
                         id = 1;
                        if (db.Inventories.Where(X=>X.Name== inventory.Name).Any())
                        {
                            id = db.Inventories.Max(x => x.id) + 1;
                        }
                        inventory.InventoryCode = Location + "-" + inventory.InventoryType + "-" + id;
                        inventory.Location = Location;
                        db.Inventories.Add(inventory);
                        db.SaveChanges();
                        inventory.Quantity--;
                    }
                }
                

                return RedirectToAction("Index");
            }

            return View(inventory);
        }

        // GET: Inventories/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Inventorytype = new SelectList(db.InventoryTypeMasters, "Type", "Type");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // POST: Inventories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Name,InventoryType,InventoryCode,Configuration,Vendor,PurchaseDate,Quantity")] Inventory inventory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inventory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventory);
        }

        // GET: Inventories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inventory inventory = db.Inventories.Find(id);
            db.Inventories.Remove(inventory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
