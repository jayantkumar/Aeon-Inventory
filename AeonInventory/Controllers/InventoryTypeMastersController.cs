﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AeonInventory.Models;

namespace AeonInventory.Controllers
{
    public class InventoryTypeMastersController : Controller
    {
        private DBContext db = new DBContext();

        // GET: InventoryTypeMasters
        public ActionResult Index()
        {
            return View(db.InventoryTypeMasters.ToList());
        }

        // GET: InventoryTypeMasters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryTypeMaster inventoryTypeMaster = db.InventoryTypeMasters.Find(id);
            if (inventoryTypeMaster == null)
            {
                return HttpNotFound();
            }
            return View(inventoryTypeMaster);
        }

        // GET: InventoryTypeMasters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InventoryTypeMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Type")] InventoryTypeMaster inventoryTypeMaster)
        {
            if (ModelState.IsValid)
            {
                db.InventoryTypeMasters.Add(inventoryTypeMaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inventoryTypeMaster);
        }

        // GET: InventoryTypeMasters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryTypeMaster inventoryTypeMaster = db.InventoryTypeMasters.Find(id);
            if (inventoryTypeMaster == null)
            {
                return HttpNotFound();
            }
            return View(inventoryTypeMaster);
        }

        // POST: InventoryTypeMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Type")] InventoryTypeMaster inventoryTypeMaster)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inventoryTypeMaster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventoryTypeMaster);
        }

        // GET: InventoryTypeMasters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryTypeMaster inventoryTypeMaster = db.InventoryTypeMasters.Find(id);
            if (inventoryTypeMaster == null)
            {
                return HttpNotFound();
            }
            return View(inventoryTypeMaster);
        }

        // POST: InventoryTypeMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InventoryTypeMaster inventoryTypeMaster = db.InventoryTypeMasters.Find(id);
            db.InventoryTypeMasters.Remove(inventoryTypeMaster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
