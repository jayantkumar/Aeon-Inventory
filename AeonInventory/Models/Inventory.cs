﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AeonInventory.Models
{
    public class Inventory
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string InventoryType { get; set; }
        public string InventoryCode { get; set; }
        public string Configuration { get; set; }
        public string Vendor { get; set; }
        public string PurchaseDate { get; set; }
        public string Location { get; set; }
        public int Quantity { get; set; }
    }
}