﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AeonInventory.Models
{
    public class LocationMaster
    {
        public int id { get; set; }
        public string Name { get; set; }
    }
}