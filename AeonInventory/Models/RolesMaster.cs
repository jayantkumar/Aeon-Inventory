﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AeonInventory.Models
{
    public class RolesMaster
    {
        public int id { get; set; }
        public string WaybillNo { get; set; }
        public string Date { get; set; }
        public int RolesAssigned { get; set; }
        public int lastRolesAssigned { get; set; }
        public int TicketsSold { get; set; }
    }
}