﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AeonInventory.Models
{
    public class InventoryTypeMaster
    {
        public int id { get; set; }
        public string Type { get; set; }

    }
}